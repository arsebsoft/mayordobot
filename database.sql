CREATE TABLE `Mastodon` (
  `id` int(11) NOT NULL,
  `last_status_id` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `Mastodon`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `Mastodon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

TRUNCATE TABLE `Mastodon`;
INSERT INTO `Mastodon` (`last_status_id`) VALUES
('1')


