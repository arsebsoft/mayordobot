## Mayordobot

Un sencillo bot para Mastodon escrito en PHP. Actualmente en desarrollo.

## Versión

0.1: versión inicial, prueba de concepto, pocos comandos por el momento.

### Instalación

Descargá la carpeta public_html, instalala en tu servidor y programá el cronjob para que ejecute `mayordobot.php` una vez por minuto.

Antes vas a necesitar configurar el archivo `config.php` con tu token e instancia.

### Comandos

* info instancia
* info primer post

### Documentación de la API de Mastodon

Podés encontrarla [acá](https://docs.joinmastodon.org/client/intro/).

### Licencia

El código es de dominio público. Hacé con él lo que quieras excepto máquinas de spam.

### Donaciones

* [ETH](https://etherscan.io/address/0xc12df5f402a8b8beaea49ed9bad9c95fccbfe907)
* [BTC](https://www.blockchain.com/btc/address/1MdfB7Pn8revosFZsoighKGd3VZ5sgjtek) 
* [Cafecito](https://cafecito.app/arielbeckerart)

### Contacto

* [Mastodon](https://mastodon.la/web/@beckermatic)
* [Email](mailto:abecker@arielbecker.com)
