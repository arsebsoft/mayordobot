<?php
/*
--------------------------------------------------------------------------------
cerebro.php
===========

Versión: 0.1

Autor: Ariel Sebastián Becker
Email: barroco@gmail.com

Descripción
===========

Clase que contiene diversas funciones útiles para un bot de Mastodon.
--------------------------------------------------------------------------------
*/

	require_once(__DIR__ . "/config.php");

	class Cerebro {
		private $token;
		private $instance_url;

		public function __construct() {
			$this->token = MASTODON_TOKEN;
			$this->instance_url = MASTODON_INSTANCE;
		}

		public function postStatus($status) {
			return $this->callMastodonAPI('/api/v1/statuses', 'POST', $status);
		}

		public function getFirstPost($id) {
			return $this->callMastodonAPI('/api/v1/accounts/' . $id . '/statuses?min_id=1&exclude_reblogs', 'GET');
		}

		public function getMastodonInstanceInfo() {
			return $this->callMastodonAPI('/api/v1/instance', 'GET');
		}

		public function getNotifications($id = "") {
			if($id > 0) {
				$lastStatus = "?since_id=" . $id;
			}
			else {
				$lastStatus = "";
			}
			return $this->callMastodonAPI('/api/v1/notifications' . $lastStatus, 'GET', null);
		}

		public function uploadMedia($media) {
			return $this->callMastodonAPI('/api/v1/media', 'POST', $media);
		}

		public function callMastodonAPI($endpoint, $method, $data = null) {
			$headers = [
				'Authorization: Bearer ' . $this->token,
				'Content-Type: multipart/form-data'
			];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, MASTODON_INSTANCE . $endpoint);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
			if($data != null) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			}
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$reply = curl_exec($ch);

			if(!$reply) {
				return null;
			}
			curl_close($ch);

			return json_decode($reply);
		}
		
		public function getLastMentionID() {
			try {
				$this->_initializeDBConn();
				$sqlQuery = "SELECT MAX(last_status_id) FROM " . DB_TABLA_ESTADOS;
				$sqlStatement = $this->sqlDBHandler->prepare($sqlQuery);
				$sqlStatement->execute();
				$retValue = $sqlStatement->fetchColumn();
			}
			catch(exception $e) {
				error_log("Error en cerebro.php: " . $e);
				$retValue = 0;
			}
			$sqlStatement = null;
			$this->_destroyDBConn();
			return $retValue;
		}

		public function checkMentionID($id) {
			try {
				$this->_initializeDBConn();
				$sqlQuery = "SELECT COUNT(id) FROM " . DB_TABLA_ESTADOS . " WHERE last_status_id = '" . $id . "'";
				$sqlStatement = $this->sqlDBHandler->prepare($sqlQuery);
				$sqlStatement->execute();
				$retValue = $sqlStatement->fetchColumn();
			}
			catch(exception $e) {
				error_log("Error en cerebro.php: " . $e);
				$retValue = 0;
			}
			$sqlStatement = null;
			$this->_destroyDBConn();
			return $retValue;
		}

		public function saveStatus($id) {
			try {
				$this->_initializeDBConn();
				$sqlQuery = "INSERT INTO " . DB_TABLA_ESTADOS . " (last_status_id) VALUES ('" . $id . "')";
				$sqlStatement = $this->sqlDBHandler->prepare($sqlQuery);
				$sqlStatement->execute();
				$retValue = true;
			}
			catch(exception $e) {
				error_log("Error en cerebro.php: " . $e);
				$retValue = false;
			}
			$sqlStatement = null;
			$this->_destroyDBConn();
			return $retValue;
		}

		private function _initializeDBConn() {
			$mysql_hostname = DB_SERVER;
			$mysql_dbname = DB_NAME;
			$mysql_password = DB_PASSWORD;
			$mysql_username = DB_USER;
			$this->sqlDBHandler = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname;charset=utf8", $mysql_username, $mysql_password);
			$this->sqlDBHandler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		private function _destroyDBConn() {
			$this->sqlDBHandler = null;
		}
	}
