<?php
	//Configuración global del servidor
	error_reporting(E_ALL);
	ini_set("log_errors", 1);
	ini_set("error_log", "./mayordobot.log");
	ini_set('display_errors', 'Off');
	ini_set('display_startup_errors', 'Off');
	date_default_timezone_set('America/Argentina/Buenos_Aires');
	
	// Definiciones globales
	define("SYS_DEBUG", "0");
	define("SYS_BASE_FOLDER", "");
	define("SYS_URL_BASE", "https://yourdomain.com/" . SYS_BASE_FOLDER);
	define("SYS_REPLIES_VISIBILITY", "public");// public , unlisted, private, and direct

	// Definición de la base de datos
	define("DB_SERVER", "localhost");
	define("DB_NAME", "dbname");
	define("DB_USER", "dbuser");
	define("DB_PASSWORD", "dbpass");
	define("DB_PORT", "3306");
	define("DB_TABLA_ESTADOS", "Mastodon");

	// Configuración general del bot
	define("MASTODON_TOKEN", "");
	define("MASTODON_INSTANCE", "https://yournodeurl.xxx");
	define("MASTODON_LANG", "es");
	
	// Cadenas regex de comandos
	define("QUERY_MENTION", "/((hola|che|decime) @mayordobot)/");
	define("QUERY_INSTANCE", "/(info instancia)/");
	define("QUERY_FIRSTPOST", "/(info primer post)/");// https://mastodon.la/api/v1/accounts/:id/statuses?min_id=1&exclude_reblogs
	
	// Misceláneos
	define("SYS_PASSPHRASE", "passphrase");
?>
