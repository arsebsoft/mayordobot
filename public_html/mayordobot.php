<?php

/*
--------------------------------------------------------------------------------
mayordobot.php
==============

Versión: 0.1

Autor: Ariel Sebastián Becker
Email: barroco@gmail.com

Descripción
===========

Procesador de peticiones sobre menciones de Mastodon.
--------------------------------------------------------------------------------
*/

	require_once(__DIR__ . "/cerebro.php");

	try {
		$objCore = new Cerebro();
		$lastMentionID = $objCore->getLastMentionID();

		if($lastMentionID > 0) {
			$arrayData = $objCore->getNotifications($lastMentionID);
			if($arrayData != null) {
				// Procesamos la lista de notificaciones, descartando las que no son menciones.
				foreach($arrayData as $notificacion) {
					if($notificacion->type == "mention") {
						if($objCore->checkMentionID($notificacion->id) == 0) { // Sólo si el toot no está guardado procedemos a responder.
							$handle = $notificacion->account->username;
							$accountID = $notificacion->account->id;
							$textoMencion = strtolower(strip_tags($notificacion->status->content));
							$statusID = $notificacion->status->id;

							$statusTextPrefix = '¡Hola @' . $handle . '! ';
							$boolResponder = false;
							$cadenas = null;
							
							if(preg_match(QUERY_INSTANCE, $textoMencion, $cadenas)) {
								// Devolvemos info básica de la instancia
								$statusText = $statusTextPrefix;
								$data = $objCore->getMastodonInstanceInfo();

								if($data != null) {
									$nombre = $data->title;
									$descripcion = $data->short_description;
									$version = $data->version;
									$email = $data->email;
									$totalUsuarios = number_format($data->stats->user_count, 0,"", " ");
									$totalToots = number_format($data->stats->status_count, 0, "", " ");

									if($data->registrations) {
										$registro = "abierta";
									}
									else {
										$registro = "cerrada";
									}
									if($data->approval_required) {
										$moderado = "requiere de aprobación previa";
									}
									else {
										$moderado = "no requiere de aprobación previa";
									}

									$statusText .= "Nuestra instancia se llama " . $nombre . ", corre la versión " . $version . ", tiene un total de " . $totalUsuarios . " usuarios y " . $totalToots . " toots; la registración está " . $registro . " y " . $moderado . ".";
								}
								else {
									$statusText .= "Error al consultar el estado de nuestra instancia. Reintentá en unos minutos por favor.";
								}

								$boolResponder = true;
							}
							
							if(preg_match(QUERY_FIRSTPOST, $textoMencion, $cadenas)) {
								// Devolvemos el primer post de la cuenta
								$statusText = $statusTextPrefix;
								$data = end($objCore->getFirstPost($accountID));

								if($data != null) {
									$statusText .= "Este es el enlace a tu primer toot: " . $data->url;
								}
								else {
									$statusText .= "Error al consultar la API. Reintentá en unos minutos por favor.";
								}

								$boolResponder = true;
							}
							
							if(preg_match(QUERY_MENTION, $textoMencion, $cadenas) && $boolResponder == false) {
								// Saludo genérico estilo Largo de la Familia Addams
								if(!$boolResponder) {
									$statusText = '¿Llamó usted, @' . $handle . '?';
									$boolResponder = true;
								}
							}
							
							if($boolResponder) {
								$status_data = [
									'status'	  => $statusText,
									'visibility'  => SYS_REPLIES_VISIBILITY,
									'language'	=> MASTODON_LANG,
									'in_reply_to_id' => $statusID
								];

								if(SYS_DEBUG == 1) {
									error_log("-----------------");
									error_log("Respuesta a ID: " . $notificacion->id);
									error_log("Handle: " . $handle);
									error_log("Respuesta: " . $statusText);
									error_log("-----------------");
								}
								$objCore->postStatus($status_data);
								$objCore->saveStatus($notificacion->id);
							}
							else {
								error_log("-----------------");
								error_log("No se responde a mención: " . $textoMencion);
								error_log("-----------------");
							}
						}
					}
				}
			}
		}
	}
	catch(exception $e) {
		error_log("Error en Mayordobot: " . $e);
	}

?>
